<?php

namespace UnicaenOauth;

return [
    'bjyauthorize' => [
        'guards' => [
            'BjyAuthorize\Guard\Controller' => [
                ['controller' => 'ZF\OAuth2\Controller\Auth', 'roles' => []],
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'login-cas' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/cas/login',
                    'defaults' => [
                        'controller' => __NAMESPACE__ . '\Controller\Cas',
                        'action'     => 'login',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            __NAMESPACE__ . '\Controller\Cas' => __NAMESPACE__ . '\Controller\CasControllerFactory',
        ],
    ],
    'service_manager' => [
        'factories' => [
            'service.cas' => __NAMESPACE__ . '\Cas\CasServiceFactory',
            'ZF\OAuth2\Adapter\PdoAdapter' => __NAMESPACE__ . '\OAuth2\Adapter\StorageAdapterFactory',
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];