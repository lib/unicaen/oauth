<?php

namespace UnicaenOauth\Controller;

use UnicaenOauth\Cas\CasServiceTrait;
use Zend\Mvc\Controller\AbstractActionController;

class CasController extends AbstractActionController
{
    use CasServiceTrait;

    public function loginAction()
    {
        $username = $this->getServiceCas()->login();

        return ['username' => $username];
    }

    public function logoutAction()
    {
        $this->getServiceCas()->logout();
        exit;
    }
}