<?php

namespace UnicaenOauth\Controller;

use Interop\Container\ContainerInterface;

class CasControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $casService = $container->get('service.cas');

        $controller = new CasController();
        $controller->setServiceCas($casService);

        return $controller;
    }
}