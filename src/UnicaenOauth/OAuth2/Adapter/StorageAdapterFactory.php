<?php

namespace UnicaenOauth\OAuth2\Adapter;

use Interop\Container\ContainerInterface;
use UnicaenOauth\Cas\CasService;
use ZF\OAuth2\Controller\Exception;

class StorageAdapterFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('Config');

        if (!isset($config['zf-oauth2']['db']) || empty($config['zf-oauth2']['db'])) {
            throw new Exception\RuntimeException(
                'The database configuration [\'zf-oauth2\'][\'db\'] for OAuth2 is missing'
            );
        }

        $username = isset($config['zf-oauth2']['db']['username']) ? $config['zf-oauth2']['db']['username'] : null;
        $password = isset($config['zf-oauth2']['db']['password']) ? $config['zf-oauth2']['db']['password'] : null;
        $options = isset($config['zf-oauth2']['db']['options']) ? $config['zf-oauth2']['db']['options'] : [];

        $oauth2ServerConfig = [];
        if (isset($config['zf-oauth2']['storage_settings']) && is_array($config['zf-oauth2']['storage_settings'])) {
            $oauth2ServerConfig = $config['zf-oauth2']['storage_settings'];
        }

        $adapter = new StorageAdapter([
            'dsn'      => $config['zf-oauth2']['db']['dsn'],
            'username' => $username,
            'password' => $password,
            'options'  => $options,
        ], $oauth2ServerConfig);

        /**
         * Inject LDAP People Service.
         */
        $ldapPeopleService = $container->get('ldapServicePeople');
        /* @var $ldapPeopleService \UnicaenLdap\Service\People */
        $adapter->setLdapService($ldapPeopleService);

        /**
         * Inject CAS service.
         */
        $casService = $container->get('service.cas');
        /* @var $casService CasService */
        $adapter->setServiceCas($casService);

        /**
         * Injecte les logins autorisés à faire de l'usurpation d'identité.
         */
        if (isset($config['unicaen-oauth']['usurpation_allowed_usernames'])) {
            $adapter->setUsurpationAllowedUsernames($config['unicaen-oauth']['usurpation_allowed_usernames']);
        }

        return $adapter;
    }
}
