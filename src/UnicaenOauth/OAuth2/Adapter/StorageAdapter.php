<?php

namespace UnicaenOauth\OAuth2\Adapter;

use UnicaenOauth\Cas\CasServiceTrait;
use UnicaenLdap\Filter\People as PeopleFilter;
use UnicaenLdap\Service\People as PeopleLdapService;
use Zend\Authentication\Adapter\Ldap as LdapAuthAdapter;
use ZF\OAuth2\Adapter\PdoAdapter;

/**
 * Extension de ZF\OAuth2\Adapter\PdoAdapter pour ajouter l'authentification LDAP et CAS.
 * Le but de ce bricolage est de remplacer l'authentification classique via la table "oauth_users"
 * par une autre façon d'authentifier :
 * - Authentification LDAP
 * Remplace l'authentification via la table "oauth_users" par une authentification LDAP.
 *
 * - Authentification CAS :
 * But: authentifier auprès du serveur CAS lorsque le password reçu commence par "cas_".
 */
class StorageAdapter extends PdoAdapter
{
    use CasServiceTrait;

    const PASSWORD_CAS_PREFIX = "cas_";
    const USURPATION_USERNAMES_SEP = '=';

    private $sourceOfAuthentification; // 'db', 'ldap' ou 'cas'

    private $usernameUsurpateur;
    private $usernameUsurpe;

    /**
     * StorageAdapter constructor.
     * @param mixed $connection
     * @param array $config
     */
    public function __construct($connection, $config = [])
    {
        parent::__construct($connection, $config);
    }

    /**
     * Redéfinition.
     *
     * Si l'authentification standard échoue (utilisateur introuvable dans la table "oauth_users" ou
     * mot de passe incorrect), on tente d'authentifier via LDAP ou via CAS.
     *
     * @param string $username Identifiant de l'utilisateur
     * @param string $password Mot de passe en clair
     * @return bool
     */
    public function checkUserCredentials($username, $password)
    {
        // si 2 logins sont fournis, cela active l'usurpation d'identité (à n'utiliser que pour les tests) :
        // - le format attendu est "loginUsurpateur=loginUsurpé"
        // - le mot de passe attendu est celui du compte usurpateur (loginUsurpateur)
        $this->usernameUsurpe = null;
        if (strpos($username, self::USURPATION_USERNAMES_SEP) > 0) {
            list($this->usernameUsurpateur, $this->usernameUsurpe) = explode(self::USURPATION_USERNAMES_SEP, $username, 2);
            if (!in_array($this->usernameUsurpateur, $this->usurpationAllowedUsernames)) {
                $this->usernameUsurpe = null;
            }
        }

        if ($user = parent::getUser($this->usernameUsurpe ?: $username)) {
            $this->sourceOfAuthentification = 'db';
            if ($this->usernameUsurpe) {
                $userUsurpateur = parent::getUser($this->usernameUsurpateur);
                $user['password'] = $userUsurpateur['password'];
            }
            return parent::checkPassword($user, $password);
        }

        if ($user = $this->getUser($this->usernameUsurpe ?: $username)) {
            if ($this->usernameUsurpe) {
                $userUsurpateur = $this->getUser($this->usernameUsurpateur);
                $user['dn'] = $userUsurpateur['dn'];
            }
            return $this->checkPassword($user, $password);
        }

        return false;
    }



    /**
     * Redéfinition.
     *
     * Au lieu d'exploiter la table "oauth_users" (mécanisme standard de cette classe), on va
     * faire soit une authentification LDAP, soit une vérification d'authentification CAS.
     *
     * Lorsque le password reçu du client commence par "cas_", cela signifie qu'une authentification CAS a déjà eu lieu,
     * que le but est donc simplement d'obtenir un token et qu'il suffit par conséquent de faire un simple
     * "checkAuthentication()" auprès du CAS.
     *
     * @param array $user Informations retournées par self::getUser() contenant notamment l'identifiant de l'utilisateur
     * @param string $password Mot de passe en clair
     * @return bool
     */
    protected function checkPassword($user, $password)
    {
        if ($this->passwordContainsCasPrefix($password)) {
            return $this->checkCasAuthentication();
        }

        return $this->checkLdapAuthentication($user['dn'], $password);
    }

    private function passwordContainsCasPrefix($password)
    {
        return substr($password, 0, strlen(self::PASSWORD_CAS_PREFIX)) === self::PASSWORD_CAS_PREFIX;
    }

    private function checkLdapAuthentication($username, $password)
    {
        $this->sourceOfAuthentification = 'ldap';

        $ldapAuthAdapter = $this->getLdapAuthAdapter();
        $ldapAuthAdapter
            ->setUsername($username)
            ->setPassword($password);
        $authResult = $ldapAuthAdapter->authenticate();

        return $authResult->isValid();
    }

    private function checkCasAuthentication()
    {
        $this->sourceOfAuthentification = 'cas';

        return $this->getServiceCas()->checkAuthentication();
    }

    /**
     * Redéfinition.
     *
     * Recherche de l'utilisateur dans l'annuaire LDAP (et non pas dans la table "oauth_users").
     *
     * @param string $username Identifiant de l'utilisateur
     * @return array|bool
     * @throws \UnicaenLdap\Exception
     */
    public function getUser($username)
    {
        $filter = PeopleFilter::username($username);
        $result = $this->getLdapService()->search($filter);
        $entity = $result->count() ? $result->current() : null; /* @var \UnicaenLdap\Entity\People */

        if (null === $entity) {
            return false;
        }

        $userInfo = array(
            'name'  => $entity->get('cn'),
            'dn'    => $entity->getDn(),
            'email' => $entity->get('mail'),
        );

        // the default behavior is to use "username" as the user_id
        return array_merge(array(
            'user_id' => $username
        ), $userInfo);
    }

    /**
     * Redéfinition nécessaire ?
     *
     * @param string $username
     * @param string $password
     * @param null $firstName
     * @param null $lastName
     * @return bool
     */
    public function setUser($username, $password, $firstName = null, $lastName = null)
    {
        throw new \BadMethodCallException("Méthode à implémenter !");
    }

    /**
     * Redéfinition.
     *
     * Teste la source de l'authentification réalisée pour bien aller chercher les infos utilisateur
     * dans la table "oauth_users" le cas échéant et pas dans l'annuaire LDAP !
     *
     * @param string $username
     * @return array|bool
     */
    public function getUserDetails($username)
    {
        if ($this->usernameUsurpe) {
            $username = $this->usernameUsurpe;
        }

        if ($this->sourceOfAuthentification === 'db') {
            return parent::getUser($username);
        }

        return $this->getUser($username);
    }

    /**
     * @var PeopleLdapService
     */
    protected $ldapService;

    /**
     * @return PeopleLdapService
     */
    public function getLdapService()
    {
        return $this->ldapService;
    }

    /**
     * @param PeopleLdapService $ldapService
     * @return self
     */
    public function setLdapService($ldapService)
    {
        $this->ldapService = $ldapService;
        return $this;
    }

    /**
     * @var LdapAuthAdapter
     */
    protected $ldapAuthAdapter;

    /**
     * @return LdapAuthAdapter
     */
    private function getLdapAuthAdapter()
    {
        if (null === $this->ldapAuthAdapter) {
            $this->ldapAuthAdapter = new LdapAuthAdapter();
            $this->ldapAuthAdapter->setLdap($this->getLdapService()->getLdap());
        }
        return $this->ldapAuthAdapter;
    }

    private $usurpationAllowedUsernames = [];

    /**
     * @param array $usernames
     */
    public function setUsurpationAllowedUsernames(array $usernames)
    {
        $this->usurpationAllowedUsernames = $usernames;
    }
}