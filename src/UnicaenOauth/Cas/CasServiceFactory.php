<?php

namespace UnicaenOauth\Cas;

use Interop\Container\ContainerInterface;

class CasServiceFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config');

        return new CasService($config['unicaen-oauth']['cas']);
    }
}