<?php

namespace UnicaenOauth\Cas;

trait CasServiceTrait
{
    /**
     * @var CasService
     */
    private $casService;

    /**
     * @param CasService $casService
     * @return $this
     */
    public function setServiceCas(CasService $casService)
    {
        $this->casService = $casService;
        return $this;
    }

    /**
     * @return CasService
     */
    public function getServiceCas()
    {
        return $this->casService;
    }
}