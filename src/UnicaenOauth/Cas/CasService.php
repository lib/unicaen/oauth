<?php
namespace UnicaenOauth\Cas;

use phpCAS;

class CasService
{
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function login()
    {
        $this->initCasClient();

        // force CAS authentication
        phpCAS::forceAuthentication();

        // at this step, the user has been authenticated by the CAS server
        // and the user's login name can be read with phpCAS::getUser().

        $username = phpCas::getUser();
//        $payload = createPayload($username);
//        $token = JWT::encode($payload, getPrivateKey(), 'HS256');

        return $username;
    }

    public function checkAuthentication()
    {
        $this->initCasClient();

        return phpCAS::checkAuthentication();
    }

    public function logout()
    {
        $this->initCasClient();

        phpCAS::logout();
        exit;
    }

    private function initCasClient()
    {
        // Enable debugging
        phpCAS::setDebug($this->config['debug']);
        // Enable verbose error messages. Disable in production!
        //phpCAS::setVerbose(true);
        // Initialize phpCAS
        phpCAS::client($this->config['version'], $this->config['host'], $this->config['port'], $this->config['uri']);
        // For production use set the CA certificate that is the issuer of the cert
        // on the CAS server and uncomment the line below
        // phpCAS::setCasServerCACert($cas_server_ca_cert_path);
        // For quick testing you can disable SSL validation of the CAS server.
        // THIS SETTING IS NOT RECOMMENDED FOR PRODUCTION.
        // VALIDATING THE CAS SERVER IS CRUCIAL TO THE SECURITY OF THE CAS PROTOCOL!
        phpCAS::setNoCasServerValidation();

        return $this;
    }

}